import {Movie, mapData} from './MovieMapper'
import { createElements, createFilmSelected } from './createElements';

export async function render(): Promise<void> {
    // TODO render your app here
    const searchButton = document.getElementById('submit');
    const searchInput = document.getElementById('search');
    const movieContainer = document.getElementById('film-container');
    const loadMore = document.getElementById('load-more');
    const popularBtn = document.getElementById('popular');
    const upcomingBtn = document.getElementById('upcoming');
    const topRatedBtn = document.getElementById('top_rated');
    const favoriteMoviesContainer = document.getElementById('favorite-movies');
    const API_KEY = 'b2120df96e03c54a14e85d740f3309b1';
    const BASE_URL = 'https://api.themoviedb.org/3/';
    const POPULAR_QUERY = 'movie/popular?api_key=';
    const UPCOMING_QUERY = 'movie/upcoming?api_key=';
    const RATED_QUERY = 'trending/all/day?api_key=';

    let idSet = new Set();

    let pageNumInc = 1;
        

     function generateCards(query: string, pageNum: unknown = 1) {
        let url = query;
        switch(query) {
            case POPULAR_QUERY:
                url = ''.concat(BASE_URL, query, API_KEY, '&page=' + pageNum);
                break;
            case UPCOMING_QUERY:
                url = ''.concat(BASE_URL, query, API_KEY, '&page=' + pageNum);
                break;
            case RATED_QUERY:
                url = ''.concat(BASE_URL, query, API_KEY, '&page=' + pageNum);
        }
        fetch(url)
        .then((data)=>data.json())
        .then((data)=>{
            const movies = data.results;
            movies.forEach((el: { poster_path: string; overview: string; release_date: string; id: number; }) => {
                (<HTMLElement>movieContainer).innerHTML += createElements(el);
            });
            return movies;
        })
        .then((data)=>{
            const cards = document.querySelectorAll('.card > svg');
            cards.forEach(item => {
                item.addEventListener('click', (e) => {
                    item.getAttribute('fill') == 'red' ? item.setAttribute('fill', 'transparent') : item.setAttribute('fill', 'red');
                    const getID = item.parentElement?.querySelector('.id')?.textContent;  
                    if(item.getAttribute('fill') == 'red') {
                        IDhelper(data, Number(getID));
                    }else {
                        deleteID(getID);
                    }
                });
            })
        })
    };

    function IDhelper(data: string | any[], cardID?: unknown) {
        idSet.add(cardID);

        // Set & read FAV IDs
        localStorage.setItem('favID', JSON.stringify(idSet));
        const stringFav = localStorage.getItem('favID');
        
        let favArray;
        if (typeof stringFav === 'string') {
            favArray = JSON.parse(stringFav);

        }
        
        (<HTMLElement>favoriteMoviesContainer).innerHTML = '';
        favArray.forEach(element => {
            const resUrl: string = 'https://api.themoviedb.org/3/movie/' + element + '?api_key=b2120df96e03c54a14e85d740f3309b1&language=en-US';

            fetch(resUrl)
                .then((data) => data.json())
                .then((data) => {
                    const movies: string = data;
                    (<HTMLElement>favoriteMoviesContainer).innerHTML += createFilmSelected(movies);
                })
        });     
            
        
    };

    function deleteID(cardID) {
        idSet.delete(cardID);
        (<HTMLElement>favoriteMoviesContainer).innerHTML = '';
        localStorage.setItem('favID', JSON.stringify(idSet));
        const stringFav = localStorage.getItem('favID');
        
        let favArray;
        if (typeof stringFav === 'string') {
            favArray = JSON.parse(stringFav);

        }


    }

    (function(){
        (<HTMLElement>movieContainer).innerHTML=" ";
        generateCards(POPULAR_QUERY);
        
    })();    
    
    (function() {
        const url = 'https://api.themoviedb.org/3/movie/popular?api_key=b2120df96e03c54a14e85d740f3309b1&language=en-US&page=1';
        fetch(url)
        .then((data) => data.json())
        .then((data)=> {
            const rand = data.results[Math.floor(Math.random() * 20)];
            (<HTMLElement>document.getElementById('random-movie-name')).textContent = rand.original_title;
            (<HTMLElement>document.getElementById('random-movie-description')).textContent = rand.overview;
        });
    })();


    (<HTMLButtonElement>searchButton).onclick = function(){
        (<HTMLElement>movieContainer).innerHTML=" ";
        const value = (<HTMLInputElement>searchInput).value;
        const url = ''.concat(BASE_URL, 'search/movie?api_key=', API_KEY, '&query=', value);
        generateCards(url);

    };
    
    (<HTMLButtonElement>loadMore).onclick = function () {
        pageNumInc++;
        if ((<HTMLInputElement>popularBtn).checked) {
            generateCards(POPULAR_QUERY, pageNumInc);

        } else if ((<HTMLInputElement>upcomingBtn).checked) {
            generateCards(UPCOMING_QUERY, pageNumInc);

        } else if ((<HTMLInputElement>topRatedBtn).checked) {
            generateCards(RATED_QUERY, pageNumInc);
        }
        
    };

    // Popular Button
    (<HTMLInputElement>popularBtn).addEventListener('click', () => {
        (<HTMLElement>movieContainer).innerHTML=" ";
        pageNumInc = 1;
        generateCards(POPULAR_QUERY);
    });

    // Upcoming Button
    (<HTMLInputElement>upcomingBtn).addEventListener('click', ()=>{
        (<HTMLElement>movieContainer).innerHTML=" ";
        pageNumInc = 1;
        generateCards(UPCOMING_QUERY);
        
    });
    
    //Top rated Button
    (<HTMLInputElement>topRatedBtn).addEventListener('click', ()=>{
        (<HTMLElement>movieContainer).innerHTML=" ";
        pageNumInc = 1;
        generateCards(RATED_QUERY);
    });

}